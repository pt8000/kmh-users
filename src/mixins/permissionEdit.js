import { required, maxLength } from "vuelidate/lib/validators";

export const permissionsMixin = {
  validations: {
    editedItem: {
      name: { required, maxLength: maxLength(30) }
    }
  },
  computed: {
    nameErrors() {
      const errors = [];
      if (!this.$v.editedItem.name.$dirty) return errors;

      !this.$v.editedItem.name.maxLength &&
        errors.push("Name must be at most 30 characters long");
      !this.$v.editedItem.name.required && errors.push("Name is required.");
      return errors;
    }
  }
};
