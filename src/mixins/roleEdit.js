import { required, maxLength } from "vuelidate/lib/validators";

export const rolesMixin = {
  validations: {
    editedItem: {
      name: { required, maxLength: maxLength(30) },
      permissions: {
        required
        // $each: {
        //   name: {
        //     required,
        //     minLength: minLength(2)
        //   }
        // }
      }
    }
  },
  computed: {
    nameErrors() {
      const errors = [];
      if (!this.$v.editedItem.name.$dirty) return errors;

      !this.$v.editedItem.name.maxLength &&
        errors.push("Name must be at most 30 characters long");
      !this.$v.editedItem.name.required && errors.push("Name is required.");
      return errors;
    },
    permissionsErrors() {
      const errors = [];
      if (!this.$v.editedItem.permissions.$dirty) return errors;

      !this.$v.editedItem.permissions.required &&
        errors.push("Permissions are required.");
      return errors;
    }
  }
};
