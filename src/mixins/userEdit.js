import { required, maxLength, email, numeric } from "vuelidate/lib/validators";

export const usersMixin = {
  validations: {
    editedItem: {
      name: { required, maxLength: maxLength(50) },
      surname: { required, maxLength: maxLength(50) },
      email: { email },
      phone: { required, numeric },
      roles: { required }
    }
  },
  computed: {
    nameErrors() {
      const errors = [];
      if (!this.$v.editedItem.name.$dirty) return errors;

      !this.$v.editedItem.name.maxLength &&
        errors.push("Name must be at most 50 characters long");
      !this.$v.editedItem.name.required && errors.push("Name is required.");
      return errors;
    },
    surnameErrors() {
      const errors = [];
      if (!this.$v.editedItem.surname.$dirty) return errors;

      !this.$v.editedItem.surname.maxLength &&
        errors.push("Surname must be at most 50 characters long");
      !this.$v.editedItem.surname.required &&
        errors.push("Surname is required.");
      return errors;
    },
    phoneErrors() {
      const errors = [];
      if (!this.$v.editedItem.phone.$dirty) return errors;

      !this.$v.editedItem.phone.numeric &&
        errors.push("Phone can contain only numbers");
      !this.$v.editedItem.phone.required && errors.push("Phone is required.");
      return errors;
    },
    emailErrors() {
      const errors = [];
      if (!this.$v.editedItem.email.$dirty) return errors;

      !this.$v.editedItem.email.email &&
        errors.push("E-Mail must be in proper format.");
      return errors;
    },
    rolesErrors() {
      const errors = [];
      if (!this.$v.editedItem.roles.$dirty) return errors;

      !this.$v.editedItem.roles.required && errors.push("Roles are required.");
      return errors;
    }
  }
};
