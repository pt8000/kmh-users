import Vue from "vue";
import Vuex from "vuex";

import SubNavigation from "@/store/modules/navigation/SubNavigation";
import Navigation from "@/store/modules/navigation/Navigation";
import Permissions from "@/store/modules/permissions/permissions";
import Roles from "@/store/modules/roles/roles";
import Users from "@/store/modules/users/users";
import MainApp from "@/store/modules/app";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    SubNavigation,
    Navigation,
    Permissions,
    Roles,
    Users,
    MainApp
  }
});
