const state = {
  title: "",
  additionalOptions: ""
};

const getters = {
  title(state) {
    return state.title;
  }
};

const mutations = {
  setSubnavigationTitle(state, title) {
    state.title = title;
  }
};

const actions = {
  setSubnavigationTitle({ commit }, title) {
    commit("setSubnavigationTitle", title);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
