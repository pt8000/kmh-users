const state = {
  menu: [
    { icon: "people", title: "User Management", link: "/" },
    { icon: "accessibility_new", title: "Roles", link: "/roles" },
    { icon: "settings", title: "Permissions", link: "/permissions" }
  ]
};

const getters = {
  menu(state) {
    return state.menu;
  }
};

const mutations = {};

const actions = {};

export default {
  state,
  getters,
  mutations,
  actions
};
