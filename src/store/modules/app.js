const state = {
  sideSearch: "",
  sidePanelActive: true,
  sideSelect: [],
  sideItems: [],
  sideFilterText: "",
  subNavButton: {
    buttonText: "",
    buttonFn: Function
  },
  loading: false,
  dialog: {
    open: false,
    buttonText: ""
  }
};

const getters = {
  sideSearch(state) {
    return state.sideSearch;
  },
  sidePanelActive(state) {
    return state.sidePanelActive;
  },
  sideSelect(state) {
    return state.sideSelect;
  },
  sideItems(state) {
    return state.sideItems;
  },
  sideFilterText(state) {
    return state.sideFilterText;
  },
  subNavButton(state) {
    return state.subNavButton;
  },
  loading(state) {
    return state.loading;
  },
  dialog(state) {
    return state.dialog;
  }
};

const mutations = {
  setSideSearch(state, payload) {
    state.sideSearch = payload;
  },
  setSidePanelActive(state, payload) {
    state.sidePanelActive = payload;
  },
  setSideSelect(state, payload) {
    state.sideSelect = payload;
  },
  setSideItems(state, payload) {
    state.sideItems = payload;
  },
  setSideFilterText(state, payload) {
    state.sideFilterText = payload;
  },
  setSubNavButton(state, payload) {
    state.subNavButton = payload;
  },
  setLoading(state, payload) {
    state.loading = payload;
  },
  setDialog(state, payload) {
    state.dialog = payload;
  }
};

const actions = {
  setSideSearch({ commit }, payload) {
    commit("setSideSearch", payload);
  },
  setSidePanelActive({ commit }, payload) {
    commit("setSidePanelActive", payload);
  },
  setSideSelect({ commit }, payload) {
    commit("setSideSelect", payload);
  },
  setSideItems({ commit }, payload) {
    commit("setSideItems", payload);
  },
  setSideFilterText({ commit }, payload) {
    commit("setSideFilterText", payload);
  },
  setSubNavButton({ commit }, payload) {
    commit("setSubNavButton", payload);
  },
  clearSubNavButtonData({ dispatch }) {
    dispatch("setSubNavButton", {
      buttonFn: Function,
      buttonText: ""
    });
  },
  setLoading({ commit }, val) {
    commit("setLoading", val);
  },
  setDialog({ commit }) {
    commit("setDialog");
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
