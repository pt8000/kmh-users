const state = {
  roles: [
    {
      name: "Administrator",
      id: "ytr",
      permissions: [
        {
          name: "Read",
          id: "sgfger"
        },
        {
          name: "Write",
          id: "bsthf"
        },
        {
          name: "Change",
          id: "dfgre"
        },
        {
          name: "Remove",
          id: "sgred"
        }
      ]
    },
    {
      name: "Writer",
      id: "nbvc",
      permissions: [
        {
          name: "Write",
          id: "bsthf"
        }
      ]
    },
    {
      name: "Editor",
      id: "n3rgf",
      permissions: [
        {
          name: "Read",
          id: "sgfger"
        },
        {
          name: "Write",
          id: "bsthf"
        }
      ]
    },
    {
      name: "Accountant",
      id: "35gtf",
      permissions: [
        {
          name: "Add",
          id: "gfrer"
        },
        {
          name: "Merge",
          id: "errre"
        }
      ]
    },
    {
      name: "Manager",
      id: "b4rg",
      permissions: [
        {
          name: "Merge",
          id: "errre"
        }
      ]
    }
  ],
  roleIdToEdit: null
};

const getters = {
  roles(state) {
    return state.roles;
  },
  roleIdToEdit(state) {
    return state.roleIdToEdit;
  },
  roleById(state) {
    return roleId => state.roles.find(item => item.id === roleId);
  }
};

const mutations = {
  createRole(state, role) {
    state.roles.push(role);
  },
  deleteRole(state, pos) {
    state.roles.splice(pos, 1);
  },
  updateRole(state, payload) {
    const roleToUpdate = state.roles.find(role => role.id === payload.id);

    roleToUpdate.name = payload.name;
    roleToUpdate.permissions = payload.permissions;
  },
  setRoleIdToEdit(state, roleId) {
    state.roleIdToEdit = roleId;
  }
};

const actions = {
  createRole({ commit, rootGetters }, role) {
    // filter whole permission objects to save with role
    const permissionsForRole = rootGetters.permissions.filter(
      item => role.permissions.indexOf(item.id) !== -1
    );

    const roleWithDate = {
      ...role,
      permissions: permissionsForRole, // update object field role.permissions with objects, not array sent from user form
      deleted: false,
      createdDate: new Date().toISOString()
    };

    commit("createRole", {
      ...roleWithDate,
      id: Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
        .substr(0, 10) // fake id, normally come from db
    });
  },
  updateRole({ commit, rootGetters }, role) {
    // filter whole permission objects to save with role
    const permissionsForRole = rootGetters.permissions.filter(
      item => role.permissions.indexOf(item.id) !== -1
    );

    commit("updateRole", {
      ...role,
      permissions: permissionsForRole
    });

    // update roles in user with edited role
    const users = rootGetters.users.filter(u =>
      u.roles.find(r => r.id === role.id)
    );

    users.forEach(user => {
      const roleToChange = user.roles.find(r => r.id === role.id);
      roleToChange.name = role.name;
    });
  },
  deleteRole({ commit, rootGetters }, role) {
    const roleLocalIndex = rootGetters.roles.findIndex(s => s.id === role.id);

    commit("deleteRole", roleLocalIndex);
  },
  setRoleIdToEdit({ commit }, roleId) {
    commit("setRoleIdToEdit", roleId);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
