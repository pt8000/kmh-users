const state = {
  permissions: [
    {
      name: "Read",
      id: "sgfger"
    },
    {
      name: "Write",
      id: "bsthf"
    },
    {
      name: "Change",
      id: "dfgre"
    },
    {
      name: "Remove",
      id: "sgred"
    },
    {
      name: "Add",
      id: "gfrer"
    },
    {
      name: "Merge",
      id: "errre"
    }
  ],
  permissionIdToEdit: null
};

const getters = {
  permissions(state) {
    return state.permissions;
  },
  permissionIdToEdit(state) {
    return state.permissionIdToEdit;
  },
  permissionById(state) {
    return permissionId =>
      state.permissions.find(item => item.id === permissionId);
  }
};

const mutations = {
  createPermission(state, permission) {
    state.permissions.push(permission);
  },
  deletePermission(state, pos) {
    state.permissions.splice(pos, 1);
  },
  updatePermission(state, payload) {
    const permissionToUpdate = state.permissions.find(
      permission => permission.id === payload.id
    );

    permissionToUpdate.name = payload.name;
  },
  setPermissionIdToEdit(state, permissionId) {
    state.permissionIdToEdit = permissionId;
  }
};

const actions = {
  createPermission({ commit }, permission) {
    const permissionWithDate = {
      ...permission,
      deleted: false,
      createdDate: new Date().toISOString()
    };

    commit("createPermission", {
      ...permissionWithDate,
      id: Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
        .substr(0, 10) // fake id, normally come from db
    });
  },
  updatePermission({ commit, rootGetters }, permission) {
    commit("updatePermission", permission);

    const roles = rootGetters.roles.filter(r =>
      r.permissions.find(p => p.id === permission.id)
    );

    roles.forEach(r => {
      const permissionToChange = r.permissions.find(
        p => p.id === permission.id
      );
      permissionToChange.name = permission.name;
    });
  },
  deletePermission({ commit, rootGetters }, permission) {
    const permissionLocalIndex = rootGetters.permissions.findIndex(
      s => s.id === permission.id
    );

    commit("deletePermission", permissionLocalIndex);
  },
  setPermissionIdToEdit({ commit }, permissionId) {
    commit("setPermissionIdToEdit", permissionId);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
