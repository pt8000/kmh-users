const state = {
  users: [
    {
      name: "Frank",
      surname: "Sinatra",
      email: "fs@test.de",
      phone: "76541234000",
      id: "4grgb",
      roles: [
        {
          name: "Administrator",
          id: "ytr"
        },
        {
          name: "Writer",
          id: "nbvc"
        },
        {
          name: "Editor",
          id: "n3rgf"
        }
      ]
    },
    {
      name: "Barry",
      surname: "White",
      email: "bw@test.de",
      phone: "098834579832222",
      id: "h43rghr",
      roles: [
        {
          name: "Accountant",
          id: "35gtf"
        },
        {
          name: "Manager",
          id: "b4rg"
        }
      ]
    },
    {
      name: "Elvis",
      surname: "Presley",
      email: "ep@test.de",
      phone: "0123445567777",
      id: "g34t355",
      roles: [
        {
          name: "Writer",
          id: "nbvc"
        },
        {
          name: "Editor",
          id: "n3rgf"
        }
      ]
    },
    {
      name: "Bob",
      surname: "Dylan",
      email: "bd@test.de",
      phone: "5558767867333",
      id: "2342df",
      roles: [
        {
          name: "Writer",
          id: "nbvc"
        }
      ]
    },
    {
      name: "Mark",
      surname: "Noname",
      email: "mn@test.de",
      phone: "64543243567",
      id: "23777gf",
      roles: [
        {
          name: "Accountant",
          id: "35gtf"
        }
      ]
    }
  ],
  userIdToEdit: null
};

const getters = {
  users(state) {
    return state.users;
  },
  userIdToEdit(state) {
    return state.userIdToEdit;
  },
  userById(state) {
    return userId => state.users.find(item => item.id === userId);
  }
};

const mutations = {
  createUser(state, user) {
    state.users.push(user);
  },
  deleteUser(state, pos) {
    state.users.splice(pos, 1);
  },
  updateUser(state, payload) {
    const userToUpdate = state.users.find(user => user.id === payload.id);

    userToUpdate.name = payload.name;
    userToUpdate.roles = payload.roles;
  },
  setUserIdToEdit(state, userId) {
    state.userIdToEdit = userId;
  }
};

const actions = {
  createUser({ commit, rootGetters }, user) {
    // filter whole permission objects to save with user
    const rolesForUser = rootGetters.roles.filter(
      item => user.roles.indexOf(item.id) !== -1
    );

    const userWithDate = {
      ...user,
      roles: rolesForUser, // update object field user.roles with objects, not array sent from user form
      deleted: false,
      createdDate: new Date().toISOString()
    };

    commit("createUser", {
      ...userWithDate,
      id: Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
        .substr(0, 10) // fake id, normally come from db
    });
  },
  updateUser({ commit, rootGetters }, user) {
    // filter whole permission objects to save with user
    const rolesForUser = rootGetters.roles.filter(
      item => user.roles.indexOf(item.id) !== -1
    );

    commit("updateUser", {
      ...user,
      roles: rolesForUser
    });
    // commit('updateUsersInUsers', user)
  },
  deleteUser({ commit, rootGetters }, user) {
    const userLocalIndex = rootGetters.users.findIndex(s => s.id === user.id);

    commit("deleteUser", userLocalIndex);
  },
  setUserIdToEdit({ commit }, userId) {
    commit("setUserIdToEdit", userId);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
