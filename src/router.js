import Vue from "vue";
import Router from "vue-router";
import Users from "./views/UserManagement.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "users",
      component: Users,
      meta: {
        breadcrumb: [{ name: "Dashboard" }]
      }
    },
    {
      path: "/roles",
      name: "roles",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Roles.vue"),
      meta: {
        breadcrumb: [{ name: "Dashboard", link: "/" }, { name: "Roles" }]
      }
    },
    {
      path: "/permissions",
      name: "permissions",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Permissions.vue"),
      meta: {
        breadcrumb: [{ name: "Dashboard", link: "/" }, { name: "Permissions" }]
      }
      // children: [

      //   {
      //     path: '/permissions/edit',
      //     component: () => import('./components/permissions/PermissionEdit.vue'),
      //     meta: {
      //       breadcrumb: [
      //         { name: 'Dashboard', link: '/' },
      //         { name: 'Permissions' },
      //         { name: 'Permissions edit' }
      //       ]
      //     }, },

      //   // ...other sub routes
      // ]
    }
  ]
});
