
## Description

User management demo with Vue.js, Vuex and Vue-router. Form validation made with Vuelidate.

1. Main components are in /view folder.
2. Child components in separate folders in /components.
3. Initial data is in store defined.
4. Communication between components uses Vuex.

## Demo

https://kmh-users.herokuapp.com/

## Clone a repository

git clone https://pt8000@bitbucket.org/pt8000/kmh-users.git


## Run on local machine

1. npm install
2. npm run serve